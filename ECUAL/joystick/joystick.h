/*
 * joystick.h
 *
 */

#ifndef JOYSTICK_JOYSTICK_H_
#define JOYSTICK_JOYSTICK_H_

#define HAL_ADC_MODULE_ENABLED

#include "stm32f4xx_hal.h"

// The Number OF JoySticks To Be Used In The Project
#define JOYSTICK_UNITS 1

typedef struct
{
    GPIO_TypeDef * JoyStick_xGPIO;
    GPIO_TypeDef * JoyStick_yGPIO;
    uint16_t       JoyStick_xPIN;
    uint16_t       JoyStick_yPIN;
    ADC_TypeDef*   ADC_Instance;
    uint32_t       ADCx_CH;
    uint32_t       ADCy_CH;
} JoyStick_CfgType;


/*----- Prototypes -----*/

/**
 * @brief Reading data from Joystick's ADC using polling method
 */
void JoyStick_Read(uint16_t JoyStick_Instance, uint16_t* JoyStick_XY);

/**
 * @brief Reading data from Joystick using DMA method
 */
void JoyStick_Read_DMA(uint16_t JoyStick_Instance, uint16_t* JoyStick_XY);

#endif /* JOYSTICK_JOYSTICK_H_ */
