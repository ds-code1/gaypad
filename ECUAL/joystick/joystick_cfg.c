/*
 * joystick_cfg.c
 *
 */

#include "joystick.h"

// JoySticks configurations
const JoyStick_CfgType JoyStick_CfgParam[JOYSTICK_UNITS] =
{
    {
        .JoyStick_xGPIO = GPIOA,
		.JoyStick_yGPIO = GPIOA,
		.JoyStick_xPIN  = GPIO_PIN_0,
		.JoyStick_yPIN  = GPIO_PIN_1,
		.ADC_Instance   = ADC1,
		.ADCx_CH        = ADC_CHANNEL_3,
		.ADCy_CH        = ADC_CHANNEL_10
    },
//	{
//		.JoyStick_xGPIO = GPIOA,
//		.JoyStick_yGPIO = GPIOA,
//		.JoyStick_xPIN  = GPIO_PIN_3,
//		.JoyStick_yPIN  = GPIO_PIN_4,
//		.ADC_Instance   = ADC1,
//		.ADCx_CH        = ADC_CHANNEL_3
//		.ADCy_CH        = ADC_CHANNEL_4
//	}
};
