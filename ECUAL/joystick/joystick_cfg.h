/*
 * joystick_cfg.h
 *
 */

#ifndef JOYSTICK_JOYSTICK_CFG_H_
#define JOYSTICK_JOYSTICK_CFG_H_

#include "JOYSTICK.h"

extern const JoyStick_CfgType JoyStick_CfgParam[JOYSTICK_UNITS];

#endif /* JOYSTICK_JOYSTICK_CFG_H_ */
