/*
 * joystick.c
 * Driver Name: Joystick
 * SW Layer: ECUAL
 *
 */

#include "JOYSTICK.h"
#include "JOYSTICK_cfg.h"

static ADC_HandleTypeDef hadc[JOYSTICK_UNITS] = {0};
static ADC_ChannelConfTypeDef sConfig = {0};


void JoyStick_Read(uint16_t JoyStick_Instance, uint16_t* JoyStick_XY)
{
	uint32_t AD_RES;
	uint32_t timeout = 1; // Ms

	// Select The JoyStick Instance ADC Channel For X
	sConfig.Channel = JoyStick_CfgParam[JoyStick_Instance].ADCx_CH;
    HAL_ADC_ConfigChannel(&hadc[JoyStick_Instance], &sConfig);
	// Start ADC Conversion
	HAL_ADC_Start(&hadc[JoyStick_Instance]);
	// Poll ADC1 Peripheral & TimeOut
	HAL_ADC_PollForConversion(&hadc[JoyStick_Instance], timeout);
	// Read The ADC Conversion Result Write It To JoyStick X
	AD_RES = HAL_ADC_GetValue(&hadc[JoyStick_Instance]);
	JoyStick_XY[0] = AD_RES;

	// Select The JoyStick Instance ADC Channel For Y
	sConfig.Channel = JoyStick_CfgParam[JoyStick_Instance].ADCy_CH;
	HAL_ADC_ConfigChannel(&hadc[JoyStick_Instance], &sConfig);
	// Start ADC Conversion
	HAL_ADC_Start(&hadc[JoyStick_Instance]);
	// Poll ADC1 Peripheral & TimeOut
	HAL_ADC_PollForConversion(&hadc[JoyStick_Instance], timeout);
	// Read The ADC Conversion Result Write It To JoyStick Y
	AD_RES = HAL_ADC_GetValue(&hadc[JoyStick_Instance]);
	JoyStick_XY[1] = AD_RES;
}


void JoyStick_Read_DMA(uint16_t JoyStick_Instance, uint16_t* JoyStick_XY)
{
	uint32_t AD_RES;
	//	 Select The JoyStick Instance ADC Channel For X
//	sConfig.Channel = JoyStick_CfgParam[JoyStick_Instance].ADCx_CH;
//    HAL_ADC_ConfigChannel(&hadc[JoyStick_Instance], &sConfig);
//
//	HAL_ADC_Start_DMA(&hadc[JoyStick_Instance], &JoyStick_XY[0], 1);
	
	//	 Select The JoyStick Instance ADC Channel For Y
	sConfig.Channel = JoyStick_CfgParam[JoyStick_Instance].ADCy_CH;
	HAL_ADC_ConfigChannel(&hadc[JoyStick_Instance], &sConfig);

	HAL_ADC_Start_DMA(&hadc[JoyStick_Instance], JoyStick_XY[1] , 1);
}
