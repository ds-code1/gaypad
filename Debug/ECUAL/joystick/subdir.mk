################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../ECUAL/joystick/joystick.c \
../ECUAL/joystick/joystick_cfg.c 

OBJS += \
./ECUAL/joystick/joystick.o \
./ECUAL/joystick/joystick_cfg.o 

C_DEPS += \
./ECUAL/joystick/joystick.d \
./ECUAL/joystick/joystick_cfg.d 


# Each subdirectory must supply rules for building sources it contributes
ECUAL/joystick/%.o ECUAL/joystick/%.su ECUAL/joystick/%.cyclo: ../ECUAL/joystick/%.c ECUAL/joystick/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F413xx -c -I../Core/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc -I../Drivers/STM32F4xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F4xx/Include -I../Drivers/CMSIS/Include -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-ECUAL-2f-joystick

clean-ECUAL-2f-joystick:
	-$(RM) ./ECUAL/joystick/joystick.cyclo ./ECUAL/joystick/joystick.d ./ECUAL/joystick/joystick.o ./ECUAL/joystick/joystick.su ./ECUAL/joystick/joystick_cfg.cyclo ./ECUAL/joystick/joystick_cfg.d ./ECUAL/joystick/joystick_cfg.o ./ECUAL/joystick/joystick_cfg.su

.PHONY: clean-ECUAL-2f-joystick

